module IVandKey (Mn, Mo, IVo, IV);
input  [127 : 0] Mn;
input  [127 : 0] Mo;
input  [127 : 0] IVo;
output  [127 : 0] IV;

wire[7 : 0] x;
reg [127 : 0] iv;
//iv = IV0;
//key address( have to be given in the main wrapper)
//assign IV [127 : 64] = 64'h123456789abcdef0;

//line counter: IV 63:56
//assign IV [63 : 56] = 8'h11;

//word counter
assign x[7] =  (Mn[127 : 112] != Mo[127 : 112]) ? (((IVo[55 : 54] + 2'b01) > 2'b11) ? 1'b1 : 1'b0 ) : 1'b0;
assign x[6] =  (Mn[111 : 96] != Mo[111 : 96])   ? (((IVo[53 : 52] + 2'b01) > 2'b11) ? 1'b1 : 1'b0 ) : 1'b0; 
assign x[5] =  (Mn[95 : 80] != Mo[95 : 80])     ? (((IVo[51 : 50] + 2'b01) > 2'b11) ? 1'b1 : 1'b0 ) : 1'b0; 
assign x[4] =  (Mn[79 : 64] != Mo[79 : 64])     ? (((IVo[49 : 48] + 2'b01) > 2'b11) ? 1'b1 : 1'b0 ) : 1'b0; 
assign x[3] =  (Mn[63 : 48] != Mo[63 : 48])     ? (((IVo[47 : 46] + 2'b01) > 2'b11) ? 1'b1 : 1'b0 ) : 1'b0; 
assign x[2] =  (Mn[47 : 32] != Mo[47 : 32])     ? (((IVo[45 : 44] + 2'b01) > 2'b11) ? 1'b1 : 1'b0 ) : 1'b0; 
assign x[1] =  (Mn[31 : 16] != Mo[31 : 16])     ? (((IVo[43 : 42] + 2'b01) > 2'b11) ? 1'b1 : 1'b0 ) : 1'b0; 
assign x[0] =  (Mn[15 : 0] != Mo[15 : 0])       ? (((IVo[41 : 40] + 2'b01) > 2'b11) ? 1'b1 : 1'b0 ) : 1'b0; 

always @(*)
begin

iv[127 : 64] = IVo [127 : 64];

if (x != 8'h0)
begin

iv [63 : 56] = IVo [63 : 56] + 8'h01;
iv [55 : 40] = 16'h0000;
end 
else
begin
iv [55 : 54] = (Mn[127 : 112] != Mo[127 : 112])  ? IVo[55 : 54] + 2'b01 : IVo[55 : 54];
iv [53 : 52] = (Mn[111 : 96] != Mo[111 : 96])   ? IVo[53 : 52] + 2'b01 : IVo[53 : 52];
iv [51 : 50] = (Mn[95 : 80] != Mo[95 : 80])     ? IVo[51 : 50] + 2'b01 : IVo[51 : 50];
iv [49 : 48] = (Mn[79 : 64] != Mo[79 : 64])     ? IVo[49 : 48] + 2'b01 : IVo[49 : 48];
iv [47 : 46] = (Mn[63 : 48 ] != Mo[63 : 48])    ? IVo[47 : 46] + 2'b01 : IVo[47 : 46];
iv [45 : 44] = (Mn[47 : 32] != Mo[47 : 32])     ? IVo[45 : 44] + 2'b01 : IVo[45 : 44];
iv [43 : 42] = (Mn[31 : 16] != Mo[31 : 16])     ? IVo[43 : 42] + 2'b01 : IVo[43 : 42];
iv [41 : 40] = (Mn[15 : 0] != Mo[15 : 0])       ? IVo[41 : 40] + 2'b01 : IVo[41 : 40];
end

//zero flag

iv [39] = (Mn[127 : 112] == 8'h0) ? 1'b0 : 1'b1;
iv [38] = (Mn[111 : 96] == 8'h0) ? 1'b0 : 1'b1;
iv [37] = (Mn[95 : 80] == 8'h0) ? 1'b0 : 1'b1;
iv [36] = (Mn[79 : 64] == 8'h0) ? 1'b0 : 1'b1;
iv [35] = (Mn[63 : 48] == 8'h0) ? 1'b0 : 1'b1;
iv [34] = (Mn[47 : 32] == 8'h0) ? 1'b0 : 1'b1;
iv [33] = (Mn[31 : 16] == 8'h0) ? 1'b0 : 1'b1;
iv [32] = (Mn[15 : 0] == 8'h0) ? 1'b0 : 1'b1;

//padding
iv[31:0] = 32'h00000000;
end
assign IV = iv;
endmodule



