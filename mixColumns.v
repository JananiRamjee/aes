module mixColumns(Sx,Sy);
output [127:0] Sy;
input  [127:0] Sx ;
wire [7:0] x [15:0];
wire [7:0] y [15:0];
assign x[0]  = Sx[7:0];
assign x[4]  = Sx[15:8];
assign x[8]  = Sx[23:16];
assign x[12] = Sx[31:24];
assign x[1]  = Sx[39:32];
assign x[5]  = Sx[47:40];
assign x[9]  = Sx[55:48];
assign x[13] = Sx[63:56];
assign x[2]  = Sx[71:64];
assign x[6]  = Sx[79:72];
assign x[10] = Sx[87:80];
assign x[14] = Sx[95:88];
assign x[3]  = Sx[103:96];
assign x[7]  = Sx[111:104];
assign x[11] = Sx[119:112];
assign x[15] = Sx[127:120];

wire [7:0] M [15:0];

assign M[0]  = 8'h02;
assign M[4]  = 8'h01;
assign M[8]  = 8'h01;
assign M[12] = 8'h03;
assign M[1]  = 8'h03;
assign M[5]  = 8'h02;
assign M[9]  = 8'h01;
assign M[13] = 8'h01;
assign M[2]  = 8'h01;
assign M[6]  = 8'h03;
assign M[10] = 8'h02;
assign M[14] = 8'h01;
assign M[3]  = 8'h01;
assign M[7]  = 8'h01;
assign M[11] = 8'h03;
assign M[15] = 8'h02;

/*function [7:0] ans (input [7:0] a,b);
reg [7:0] x1;
reg [7:0] y1;
reg [7:0] z;
begin
if(b == 8'h03)
begin
 x1 = a << 1;
 y1 = x1 ^ a;
 z = (a[7]==1) ? (y1 ^ 8'h1b) : y1;
end
else if (b == 8'h02)
begin
 x1 = a << 1;
 z = (a[7]==1) ? (y1 ^ 8'h1b) : y1;
end 
else
 z = a; 
ans = z;
end
endfunction */
wire [7 : 0] l [63 : 0];
ans l0 (x[0],M[0],l[0]);
ans l1 (x[1],M[1],l[1]);
ans l2 (x[2],M[2],l[2]);
ans l3 (x[3],M[3],l[3]);
assign y[0]=  l[0]  ^ l[1]    ^ l[2]   ^ l[3];
ans l4 (x[0],M[4],l[4]);
ans l5 (x[1],M[5],l[5]);
ans l6 (x[2],M[6],l[6]);
ans l7 (x[3],M[7],l[7]);
assign y[1]=  l[4]   ^ l[5]   ^ l[6]   ^ l[7];
ans l8  (x[0],M[8],l[8]);
ans l9  (x[1],M[9],l[9]);
ans l10 (x[2],M[10],l[10]);
ans l11 (x[3],M[11],l[11]);
assign y[2]=  l[8]   ^ l[9]   ^ l[10]  ^ l[11];
ans l12  (x[0],M[12],l[12]);
ans l13  (x[1],M[13],l[13]);
ans l14 (x[2],M[14],l[14]);
ans l15 (x[3],M[15],l[15]);
assign y[3]=  l[12]  ^ l[13]  ^ l[14]  ^ l[15];
ans l16  (x[4],M[0],l[16]);
ans l17  (x[5],M[1],l[17]);
ans l18  (x[6],M[2],l[18]);
ans l19  (x[7],M[3],l[19]);
assign y[4]=  l[16]   ^ l[17]   ^ l[18]   ^ l[19];
ans l20  (x[4],M[4],l[20]);
ans l21  (x[5],M[5],l[21]);
ans l22  (x[6],M[6],l[22]);
ans l23  (x[7],M[7],l[23]);
assign y[5]=  l[20]   ^ l[21]   ^ l[22]   ^ l[23];
ans l24  (x[4],M[8],l[24]);
ans l25  (x[5],M[9],l[25]);
ans l26  (x[6],M[10],l[26]);
ans l27  (x[7],M[11],l[27]);
assign y[6]=  l[24]   ^ l[25]   ^ l[26]  ^ l[27];
ans l28  (x[4],M[12],l[28]);
ans l29  (x[5],M[13],l[29]);
ans l30  (x[6],M[14],l[30]);
ans l31  (x[7],M[15],l[31]);
assign y[7]=  l[28]  ^ l[29]  ^ l[30]  ^ l[31];
ans l32  (x[8],M[0],l[32]);
ans l33  (x[9],M[1],l[33]);
ans l34  (x[10],M[2],l[34]);
ans l35  (x[11],M[3],l[35]);
assign y[8]=  l[32]   ^ l[33]   ^ l[34]  ^ l[35];
ans l36  (x[8],M[4],l[36]);
ans l37  (x[9],M[5],l[37]);
ans l38  (x[10],M[6],l[38]);
ans l39  (x[11],M[7],l[39]);
assign y[9]=  l[36]   ^ l[37]   ^ l[38]  ^ l[39];
ans l40  (x[8],M[8],l[40]);
ans l41  (x[9],M[9],l[41]);
ans l42  (x[10],M[10],l[42]);
ans l43  (x[11],M[11],l[43]);
assign y[10]= l[40]   ^ l[41]   ^ l[42] ^ l[43];
ans l44  (x[8],M[12],l[44]);
ans l45  (x[9],M[13],l[45]);
ans l46  (x[10],M[14],l[46]);
ans l47  (x[11],M[15],l[47]);
assign y[11]= l[44]  ^ l[45]  ^ l[46] ^ l[47];
ans l48  (x[12],M[0],l[48]);
ans l49  (x[13],M[1],l[49]);
ans l50  (x[14],M[2],l[50]);
ans l51  (x[15],M[3],l[51]);
assign y[12]= l[48]  ^ l[49]  ^ l[50]  ^ l[51];
ans l52  (x[12],M[4],l[52]);
ans l53  (x[13],M[5],l[53]);
ans l54  (x[14],M[6],l[54]);
ans l55  (x[15],M[7],l[55]);
assign y[13]= l[52]  ^ l[53]  ^ l[54]  ^ l[55];
ans l56  (x[12],M[8],l[56]);
ans l57  (x[13],M[9],l[57]);
ans l58  (x[14],M[10],l[58]);
ans l59  (x[15],M[11],l[59]);
assign y[14]= l[56]  ^ l[57]  ^ l[58] ^ l[59];
ans l60  (x[12],M[12],l[60]);
ans l61  (x[13],M[13],l[61]);
ans l62  (x[14],M[14],l[62]);
ans l63  (x[15],M[15],l[63]);
assign y[15]= l[60] ^ l[61] ^ l[62] ^ l[63];

assign Sy[7:0]      = y[0];
assign Sy[15:8]     = y[4];
assign Sy[23:16]    = y[8];
assign Sy[31:24]    = y[12];
assign Sy[39:32]    = y[1];
assign Sy[47:40]    = y[5];
assign Sy[55:48]    = y[9];
assign Sy[63:56]    = y[13];
assign Sy[71:64]    = y[2];
assign Sy[79:72]    = y[6];
assign Sy[87:80]    = y[10];
assign Sy[95:88]    = y[14];
assign Sy[103:96]   = y[3];
assign Sy[111:104]  = y[7];
assign Sy[119:112]  = y[11];
assign Sy[127:120]  = y[15];

endmodule


 
