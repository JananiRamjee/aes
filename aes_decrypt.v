module aes_decrypt(C,M,Ks0,Ks1,Ks2,Ks3,Ks4,Ks5,Ks6,Ks7,Ks8,Ks9,Ks10,inv);
input  [127 : 0] C,Ks0,Ks1,Ks2,Ks3,Ks4,Ks5,Ks6,Ks7,Ks8,Ks9,Ks10;
input inv;
output [127 : 0] M;


addRoundKey ark0 (.St(S1),.S(C),.K(Ks0));
//1
shiftRowInv sri1 (.S1(S1), .S2(S2));
InvSbox is1 (.S1(S2), .S2(S3));
addRoundKey ark1 (.St(S4), .S(S3), .K(Ks1));
InvMixColumns imc1 (.S1(S4), .S2(S5));
//2
shiftRowInv sri2 (.S1(S5), .S2(S6));
InvSbox is2 (.S1(S6), .S2(S7));
addRoundKey ark2 (.St(S8), .S(S7), .K(Ks2));
InvMixColumns imc2 (.S1(S8), .S2(S9));
//3
shiftRowInv sri3 (.S1(S9), .S2(S10));
InvSbox is3 (.S1(S10), .S2(S11));
addRoundKey ark3 (.St(S12), .S(S11), .K(Ks3));
InvMixColumns imc3 (.S1(S12), .S2(S13));
//4
shiftRowInv sri4 (.S1(S13), .S2(S14));
InvSbox is4 (.S1(S14), .S2(S15));
addRoundKey ark4 (.St(S16), .S(S15), .K(Ks4));
InvMixColumns imc4 (.S1(S16), .S2(S17));
//5
shiftRowInv sri5 (.S1(S17), .S2(S18));
InvSbox is5 (.S1(S18), .S2(S19));
addRoundKey ark5 (.St(S20), .S(S19), .K(Ks5));
InvMixColumns imc5 (.S1(S20), .S2(S21));
//6
shiftRowInv sri6 (.S1(S21), .S2(S22));
InvSbox is6 (.S1(S22), .S2(S23));
addRoundKey ark6 (.St(S24), .S(S23), .K(Ks6));
InvMixColumns imc6 (.S1(S24), .S2(S25));
//7
shiftRowInv sri7 (.S1(S25), .S2(S26));
InvSbox is7 (.S1(S26), .S2(S27));
addRoundKey ark7 (.St(S28), .S(S27), .K(Ks7));
InvMixColumns imc7 (.S1(S28), .S2(S29));
//8
shiftRowInv sri8 (.S1(S29), .S2(S30));
InvSbox is8 (.S1(S30), .S2(S31));
addRoundKey ark8 (.St(S32), .S(S31), .K(Ks8));
InvMixColumns imc8 (.S1(S32), .S2(S33));
//9
shiftRowInv sri9 (.S1(S33), .S2(S34));
InvSbox is9 (.S1(S34), .S2(S35));
addRoundKey ark9 (.St(S36), .S(S35), .K(Ks9));
InvMixColumns imc9 (.S1(S36), .S2(S37));
//last round
shiftRowInv sri10 (.S1(S37), .S2(S38));
InvSbox is10 (.S1(S38), .S2(S39));
addRoundKey ark10 (.St(S40), .S(S39), .K(Ks10));

reg [127:0] I;

always @(inv)
begin
if (inv == 1'b1)
I = ~(S40);
else I = S40;
end

assign M = I;

endmodule
