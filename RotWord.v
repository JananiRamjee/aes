module RotWord ( t1, t2 );
input  [31 : 0] t1;
output [31 : 0] t2;

assign t2 [31 : 24] = t1 [23 : 16];
assign t2 [23 : 16] = t1 [15 : 8];
assign t2 [15 : 8]  = t1 [7 : 0];
assign t2 [7 : 0] = t1 [31 : 24];

endmodule 
