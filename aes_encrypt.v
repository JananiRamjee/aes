module aes (M, K, C, P, invt);
input  [127 : 0] M;
input  [127 : 0] K;
input  [127 : 0] P;
output [127 : 0] C;
output invt;

wire [127 : 0] Keys [10 : 0];

keySchedule Key(.Ck(C), .Ks0(Keys[0]), .Ks1(Keys[1]), .Ks2(Keys[2]), .Ks3(Keys[3]), .Ks4(Keys[4]), .Ks5(Keys[5]), .Ks6(Keys[6]), .Ks7(Keys[7]), .Ks8(Keys[8]), .Ks9(Keys[9]), .Ks10(Keys[10]));

wire [127 : 0] S0;
wire [127 : 0] S1;
wire [127 : 0] S2;
wire [127 : 0] S3;
wire [127 : 0] S4;
wire [127 : 0] S5;
wire [127 : 0] S6;
wire [127 : 0] S7;
wire [127 : 0] S8;
wire [127 : 0] S9;
wire [127 : 0] S10;
wire [127 : 0] S11;
wire [127 : 0] S12;
wire [127 : 0] S13;
wire [127 : 0] S14;
wire [127 : 0] S15;
wire [127 : 0] S16;
wire [127 : 0] S17;
wire [127 : 0] S18;
wire [127 : 0] S19;
wire [127 : 0] S20;
wire [127 : 0] S21;
wire [127 : 0] S22;
wire [127 : 0] S23;
wire [127 : 0] S24;
wire [127 : 0] S25;
wire [127 : 0] S26;
wire [127 : 0] S27;
wire [127 : 0] S28;
wire [127 : 0] S29;
wire [127 : 0] S30;
wire [127 : 0] S31;
wire [127 : 0] S32;
wire [127 : 0] S33;
wire [127 : 0] S34;
wire [127 : 0] S35;
wire [127 : 0] S36;
wire [127 : 0] S37;
wire [127 : 0] S38;
wire [127 : 0] S39;
wire [127 : 0] S40;
wire [127 : 0] S41;


addRoundKey Ark1(.St(S1),.S(M),.K(Keys[0]));
//1
Sbox Sb1(.S1(S1),.S2(S2));
shiftRows Sr1(.S2(S3),.S1(S2));
mixColumns Mc1(.Sy(S4),.Sx(S3));
addRoundKey Ark2(.St(S5),.S(S4),.K(Keys[1]));
//2
Sbox Sb2(.S1(S5),.S2(S6));
shiftRows Sr2(.S2(S7),.S1(S6));
mixColumns Mc2(.Sy(S8),.Sx(S7));
addRoundKey Ark3(.St(S9),.S(S8),.K(Keys[2]));
//3
Sbox Sb3(.S1(S9),.S2(S10));
shiftRows Sr3(.S2(S11),.S1(S10));
mixColumns Mc3(.Sy(S12),.Sx(S11));
addRoundKey Ark4(.St(S13),.S(S12),.K(Keys[3]));
//4
Sbox Sb4(.S1(S13),.S2(S14));
shiftRows Sr4(.S2(S15),.S1(S14));
mixColumns Mc4(.Sy(S16),.Sx(S15));
addRoundKey Ark5(.St(S17),.S(S16),.K(Keys[4]));
//5
Sbox Sb5(.S1(S17),.S2(S18));
shiftRows Sr5(.S2(S19),.S1(S18));
mixColumns Mc5(.Sy(S20),.Sx(S19));
addRoundKey Ark6(.St(S21),.S(S20),.K(Keys[5]));
//6
Sbox Sb6(.S1(S21),.S2(S22));
shiftRows Sr6(.S2(S23),.S1(S22));
mixColumns Mc6(.Sy(S24),.Sx(S23));
addRoundKey Ark7(.St(S25),.S(S24),.K(Keys[6]));
//7
Sbox Sb7(.S1(S25),.S2(S39));
shiftRows Sr7(.S2(S26),.S1(S39));
mixColumns Mc7(.Sy(S27),.Sx(S26));
addRoundKey Ark8(.St(S28),.S(S27),.K(Keys[7]));
//8
Sbox Sb8(.S1(S28),.S2(S29));
shiftRows Sr8(.S2(S30),.S1(S29));
mixColumns Mc8(.Sy(S31),.Sx(S30));
addRoundKey Ark9(.St(S32),.S(S31),.K(Keys[8]));
//9
Sbox Sb9(.S1(S32),.S2(S33));
shiftRows Sr9(.S2(S34),.S1(S33));
mixColumns Mc9(.Sy(S35),.Sx(S34));
addRoundKey Ark10(.St(S36),.S(S35),.K(Keys[9]));
//last round
Sbox Sb10(.S1(S36),.S2(S37));
shiftRows Sr10(.S2(S38),.S1(S37));
addRoundKey Ark11(.St(S40),.S(S38),.K(Keys[10]));

wire [127 : 0] rcheck,P;//the line in the address which is already present.
assign rcheck = 128'h12343421123434211234342112343421;

assign S41 = S40 ^ P;

hammingcode hc(.CT(S41), .OCT(rcheck), .FCT(C), .inv(invt));
endmodule

