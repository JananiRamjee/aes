module invans(a,b,z);
input [7:0] a,b;
output [7:0] z;
function [7 : 0] a2(input [7 : 0] xy);
    begin
      a2 = {xy[6 : 0], 1'b0} ^ (8'h1b & {8{xy[7]}});
    end
  endfunction // a2

  function [7 : 0] a3(input [7 : 0] xy);
    begin
      a3 = a2(xy) ^ xy;
    end
  endfunction // a3

  function [7 : 0] a4(input [7 : 0] xy);
  reg [7:0] x;
    begin
	  x = {xy[6 : 0], 1'b0} ^ (8'h1b & {8{xy[7]}});
      a4 = {x[6 : 0], 1'b0} ^ (8'h1b & {8{x[7]}});
    end
  endfunction // a4

  function [7 : 0] a8(input [7 : 0] xy);
  reg [7 : 0] d,e;
    begin
	  d = {xy[6 : 0], 1'b0} ^ (8'h1b & {8{xy[7]}});
      e = {d[6 : 0], 1'b0} ^ (8'h1b & {8{d[7]}});
      a8 = {e[6 : 0], 1'b0} ^ (8'h1b & {8{e[7]}});
    end
  endfunction // a8

  function [7 : 0] a9(input [7 : 0] xy);
    begin
      a9 = a8(xy) ^ xy;
    end
  endfunction // a09

  function [7 : 0] a11(input [7 : 0] xy);
    begin
      a11 = a8(xy) ^ a2(xy) ^ xy;
    end
  endfunction // a11

  function [7 : 0] a13(input [7 : 0] xy);
    begin
      a13 = a8(xy) ^ a4(xy) ^ xy;
    end
  endfunction // a13

  function [7 : 0] a14(input [7 : 0] xy);
    begin
a14 = a8(xy) ^ a4(xy) ^ a2(xy);
    end
  endfunction // 14

reg [7 : 0] r;

always @(b)
begin
if(b==8'h0e)
r = a14(a);
else if (b==8'h0d)
r = a13(a);
else if (b==8'h0b)
r = a11(a);
else
r = a9(a);
end

assign z = r;

endmodule
