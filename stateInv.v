module stateInv (S, C);
input [7 : 0]S[15 : 0];
output [127 : 0] C;

assign C[7:0]      = S[0];
assign C[15:8]     = S[4];
assign C[23:16]    = S[8];
assign C[31:24]    = S[12];
assign C[39:32]    = S[1];
assign C[47:40]    = S[5];
assign C[55:48]    = S[9];
assign C[63:56]    = S[13];
assign C[71:64]    = S[2];
assign C[79:72]    = S[6];
assign C[87:80]    = S[10];
assign C[95:88]    = S[14];
assign C[103:96]   = S[3];
assign C[111:104]  = S[7];
assign C[119:112]  = S[11];
assign C[127:120]  = S[15];
endmodule 
