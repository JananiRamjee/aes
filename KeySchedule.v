module keySchedule (Ck, Ks0, Ks1, Ks2, Ks3, Ks4, Ks5, Ks6, Ks7, Ks8, Ks9, Ks10);
input [127 : 0] Ck;
output [127 : 0] Ks0, Ks1, Ks2, Ks3, Ks4, Ks5, Ks6, Ks7, Ks8, Ks9, Ks10;

wire [31 : 0] w [43 : 0];
wire [31 : 0] temp1,temp2,temp3,temp4;
wire [31 : 0] t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15;
wire [31 : 0] t16,t17,t18,t19,t20,t21,t22,t23,t24,t25,t26,t27;
assign w[0] = Ck [31 : 0];
assign w[1] = Ck [63 : 32];
assign w[2] = Ck [95 : 64];
assign w[3] = Ck [127 : 96];

RotWord X1(w[3], temp2);
SubWord Y1(temp2, temp3);
assign temp4 = temp3 ^ 32'h01000000;
assign w[4] = temp4 ^ w[0];
assign w[5] = w[4] ^ w[1];
assign w[6] = w[5] ^ w[2];
assign w[7] = w[6] ^ w[3];

RotWord X2(w[7],t1);
SubWord Y2(t1,t2);
assign t3 = t2 ^ 32'h02000000;
assign w[8] = t3 ^ w[4];
assign w[9] = w[8] ^ w[5];
assign w[10] = w[9] ^ w[6];
assign w[11] = w[10] ^ w[7];

RotWord X3(w[11],t4);
SubWord Y3(t4,t5);
assign t6 = t5 ^ 32'h04000000;
assign w[12] = t6 ^ w[8];
assign w[13] = w[12] ^ w[9];
assign w[14] = w[13] ^ w[10];
assign w[15] = w[14] ^ w[11];

RotWord X4(w[15],t7);
SubWord Y4(t7,t8);
assign t9 = t8 ^ 32'h08000000;
assign w[16] = t9 ^ w[12];
assign w[17] = w[16] ^ w[13];
assign w[18] = w[17] ^ w[14];
assign w[19] = w[18] ^ w[15];

RotWord X5(w[19],t10);
SubWord Y5(t10,t11);
assign t12 = t11 ^ 32'h10000000;
assign w[20] = t12 ^ w[16];
assign w[21] = w[20] ^ w[17];
assign w[22] = w[21] ^ w[18];
assign w[23] = w[22] ^ w[19];

RotWord X6(w[23],t13);
SubWord Y6(t13,t14);
assign t15 = t14 ^ 32'h20000000;
assign w[24] = t15 ^ w[20];
assign w[25] = w[24] ^ w[21];
assign w[26] = w[25] ^ w[22];
assign w[27] = w[26] ^ w[23];

RotWord X7(w[27],t16);
SubWord Y7(t16,t17);
assign t18 = t17 ^ 32'h40000000;
assign w[28] = t18 ^ w[24];
assign w[29] = w[28] ^ w[25];
assign w[30] = w[29] ^ w[26];
assign w[31] = w[30] ^ w[27];

RotWord X8(w[31],t19);
SubWord Y8(t19,t20);
assign t21 = t20 ^ 32'h80000000;
assign w[32] = t21 ^ w[28];
assign w[33] = w[32] ^ w[29];
assign w[34] = w[33] ^ w[30];
assign w[35] = w[34] ^ w[31];

RotWord X9(w[35],t22);
SubWord Y9(t22,t23);
assign t24 = t23 ^ 32'h1b000000;
assign w[36] = t24 ^ w[32];
assign w[37] = w[36] ^ w[33];
assign w[38] = w[37] ^ w[34];
assign w[39] = w[38] ^ w[35];

RotWord X10(w[39],t25);
SubWord Y10(t25,t26);
assign t27 = t26 ^ 32'h36000000;
assign w[40] = t27 ^ w[36];
assign w[41] = w[40] ^ w[37];
assign w[42] = w[41] ^ w[38];
assign w[43] = w[42] ^ w[39];

assign Ks0  = {w[0], w[1], w[2], w[3]};
assign Ks1  = {w[4], w[5], w[6], w[7]};
assign Ks2  = {w[8], w[9], w[10], w[11]};
assign Ks3  = {w[12], w[13], w[14], w[15]};
assign Ks4  = {w[16], w[17], w[18], w[19]};
assign Ks5  = {w[20], w[21], w[22], w[23]};
assign Ks6  = {w[24], w[25], w[26], w[27]};
assign Ks7  = {w[28], w[29], w[30], w[31]};
assign Ks8  = {w[32], w[33], w[34], w[35]};
assign Ks9  = {w[36], w[37], w[38], w[39]};
assign Ks10 = {w[40], w[41], w[42], w[43]};

endmodule


 


