module aes_write(M,n,Key,Cip,in);
input [7 : 0] n;
input [127 : 0] M;
input [127 : 0] Key;
output in;
output [127 : 0] Cip;
reg [127 : 0] IV [8 : 0];
//reg [63  : 0] add;
//reg [7 : 0] lc;

initial begin
$readmemh("mem.hex",IV);
end

wire [127 : 0] ch;
reg [127 : 0] check0,check1,check2,check3,check4,check5,check6,check7,check8,checkf,c1,c2, c3, c4, c5, c6, c7;
assign ch = IV[n];

always @(ch)
begin
if(ch[63 : 56] >= 8'h33)
begin
check0 = IV[0];
check1 = IV[1];
check2 = IV[2];
check3 = IV[3];
check4 = IV[4];
check5 = IV[5];
check6 = IV[6];
check7 = IV[7];
check8 = IV[8];

c1 = (check0[63:56]<check1[63:56])? check0 : check1;
c2 = (c1[63:56]<check2[63:56]) ? c1 : check2;
c3 = (c2[63:56]<check3[63:56]) ? c2 : check3;
c4 = (c3[63:56]<check4[63:56]) ? c3 : check4;
c5 = (c4[63:56]<check5[63:56]) ? c4 : check5;
c6 = (c5[63:56]<check6[63:56]) ? c5 : check6;
c7 = (c6[63:56]<check7[63:56]) ? c6 : check7;
checkf = (c7[63:56]<check8[63:56]) ? c7 : check8;
end
else
checkf = IV[n];
checkf[63 : 56] = (checkf==ch) ? 8'h00 : ch[63 : 56];
end

wire [127 : 0] mOld, ctr;
assign mOld = 127'h345eabcd345eabcd345eabcd345eabcd;


IVandKey IVK (.Mn(M), .Mo(mOld), .IVo(checkf), .IV(ctr));

aes A (.M(ctr), .K(Key), .C(Cip), .P(M), .invt(in));

endmodule








