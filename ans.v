module ans(a,b,z);
input [7:0] a,b;
output [7:0] z;
reg[7:0] r;
function [7 : 0] a2(input [7 : 0] xy);
    begin
      a2 = {xy[6 : 0], 1'b0} ^ (8'h1b & {8{xy[7]}});
    end
  endfunction // a2

  function [7 : 0] a3(input [7 : 0] xy);
    begin
      a3 = a2(xy) ^ xy;
    end
  endfunction // a3

  function [7 : 0] a4(input [7 : 0] xy);
  reg [7:0] x;
    begin
	  x = {xy[6 : 0], 1'b0} ^ (8'h1b & {8{xy[7]}});
      a4 = {x[6 : 0], 1'b0} ^ (8'h1b & {8{x[7]}});
    end
  endfunction // a4
always @(b)
begin
if(b==8'h02)
r = a2(a);
else if (b==8'h03)
r = a3(a);
else
r = a4(a);
end

assign z = r;

endmodule
