module addRoundKey (St,S,K);
output [127:0] St;
input  [127:0] S ;
input  [127:0] K;
wire [7:0] x [15:0];
wire [7:0] y [15:0];
assign x[0]  = S[7:0];
assign x[4]  = S[15:8];
assign x[8]  = S[23:16];
assign x[12] = S[31:24];
assign x[1]  = S[39:32];
assign x[5]  = S[47:40];
assign x[9]  = S[55:48];
assign x[13] = S[63:56];
assign x[2]  = S[71:64];
assign x[6]  = S[79:72];
assign x[10] = S[87:80];
assign x[14] = S[95:88];
assign x[3]  = S[103:96];
assign x[7]  = S[111:104];
assign x[11] = S[119:112];
assign x[15] = S[127:120];
assign y[0]=  K[7:0]    ^   x[0] ;
assign y[4]=  K[15:8]   ^   x[4] ;
assign y[8]=  K[23:16]  ^   x[8];
assign y[12]= K[31:24]  ^   x[12];
assign y[1]=  K[39:32]  ^   x[1];
assign y[5]=  K[47:40]  ^   x[5];
assign y[9]=  K[55:48]  ^   x[9];
assign y[13]= K[63:56]  ^   x[13];
assign y[2]=  K[71:64]  ^   x[2];
assign y[6]=  K[79:72]  ^   x[6];
assign y[10]= K[87:80]  ^   x[10];
assign y[14]= K[95:88]  ^   x[14];
assign y[3]=  K[103:96] ^   x[3];
assign y[7]=  K[111:104]^   x[7];
assign y[11]= K[119:112]^   x[11];
assign y[15]= K[127:120]^   x[15];
assign St[7:0]      = y[0];
assign St[15:8]     = y[4];
assign St[23:16]    = y[8];
assign St[31:24]    = y[12];
assign St[39:32]    = y[1];
assign St[47:40]    = y[5];
assign St[55:48]    = y[9];
assign St[63:56]    = y[13];
assign St[71:64]    = y[2];
assign St[79:72]    = y[6];
assign St[87:80]    = y[10];
assign St[95:88]    = y[14];
assign St[103:96]   = y[3];
assign St[111:104]  = y[7];
assign St[119:112]  = y[11];
assign St[127:120]  = y[15];
endmodule
