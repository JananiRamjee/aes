module hammingcode (CT, OCT, FCT, inv);

input  [127 : 0] CT;
input  [127 : 0] OCT;
output [127 : 0] FCT;
output inv;

wire  [7 : 0] count [127 : 0];

//assign count[127] = 8'h00;

assign count[127] = (CT[127] != OCT[127]) ? 8'h01 : 8'h00; 
assign count[126] = (CT[126] != OCT[126]) ? count[127] + 8'h01 : count[127] + 8'h00; 
assign count[125] = (CT[125] != OCT[125]) ? count[126] + 8'h01 : count[126] + 8'h00; 
assign count[124] = (CT[124] != OCT[124]) ? count[125] + 8'h01 : count[125] + 8'h00;
assign count[123] = (CT[123] != OCT[123]) ? count[124] + 8'h01 : count[124] + 8'h00; 
assign count[122] = (CT[122] != OCT[122]) ? count[123] + 8'h01 : count[123] + 8'h00; 
assign count[121] = (CT[121] != OCT[121]) ? count[122] + 8'h01 : count[122] + 8'h00; 
assign count[120] = (CT[120] != OCT[120]) ? count[121] + 8'h01 : count[121] + 8'h00; 
assign count[119] = (CT[119] != OCT[119]) ? count[120] + 8'h01 : count[120] + 8'h00; 
assign count[118] = (CT[118] != OCT[118]) ? count[119] + 8'h01 : count[119] + 8'h00; 
assign count[117] = (CT[117] != OCT[117]) ? count[118] + 8'h01 : count[118] + 8'h00; 
assign count[116] = (CT[116] != OCT[116]) ? count[117] + 8'h01 : count[117] + 8'h00; 
assign count[115] = (CT[115] != OCT[115]) ? count[116] + 8'h01 : count[116] + 8'h00; 
assign count[114] = (CT[114] != OCT[114]) ? count[115] + 8'h01 : count[115] + 8'h00; 
assign count[113] = (CT[113] != OCT[113]) ? count[114] + 8'h01 : count[114] + 8'h00; 
assign count[112] = (CT[112] != OCT[112]) ? count[113] + 8'h01 : count[113] + 8'h00; 
assign count[111] = (CT[111] != OCT[111]) ? count[112] + 8'h01 : count[112] + 8'h00; 
assign count[110] = (CT[110] != OCT[110]) ? count[111] + 8'h01 : count[111] + 8'h00; 
assign count[109] = (CT[109] != OCT[109]) ? count[110] + 8'h01 : count[110] + 8'h00; 
assign count[108] = (CT[108] != OCT[108]) ? count[109] + 8'h01 : count[109] + 8'h00; 
assign count[107] = (CT[107] != OCT[107]) ? count[108] + 8'h01 : count[108] + 8'h00; 
assign count[106] = (CT[106] != OCT[106]) ? count[107] + 8'h01 : count[107] + 8'h00; 
assign count[105] = (CT[105] != OCT[105]) ? count[106] + 8'h01 : count[106] + 8'h00; 
assign count[104] = (CT[104] != OCT[104]) ? count[105] + 8'h01 : count[105] + 8'h00; 
assign count[103] = (CT[103] != OCT[103]) ? count[104] + 8'h01 : count[104] + 8'h00; 
assign count[102] = (CT[102] != OCT[102]) ? count[103] + 8'h01 : count[103] + 8'h00; 
assign count[101] = (CT[101] != OCT[101]) ? count[102] + 8'h01 : count[102] + 8'h00; 
assign count[100] = (CT[100] != OCT[100]) ? count[101] + 8'h01 : count[101] + 8'h00; 
assign count[99]  = (CT[99]  != OCT[99])  ? count[100] + 8'h01 : count[100] + 8'h00; 
assign count[98]  = (CT[98]  != OCT[98])  ? count[99] + 8'h01 : count[99] + 8'h00; 
assign count[97]  = (CT[97]  != OCT[97])  ? count[98] + 8'h01 : count[98] + 8'h00; 
assign count[96]  = (CT[96]  != OCT[96])  ? count[97] + 8'h01 : count[97] + 8'h00; 
assign count[95]  = (CT[95]  != OCT[95])  ? count[96] + 8'h01 : count[96] + 8'h00; 
assign count[94]  = (CT[94]  != OCT[94])  ? count[95] + 8'h01 : count[95] + 8'h00; 
assign count[93]  = (CT[93]  != OCT[93])  ? count[94] + 8'h01 : count[94] + 8'h00; 
assign count[92]  = (CT[92]  != OCT[92])  ? count[93] + 8'h01 : count[93] + 8'h00; 
assign count[91]  = (CT[91]  != OCT[91])  ? count[92] + 8'h01 : count[92] + 8'h00; 
assign count[90]  = (CT[90]  != OCT[90])  ? count[91] + 8'h01 : count[91] + 8'h00; 
assign count[89]  = (CT[89]  != OCT[89])  ? count[90] + 8'h01 : count[90] + 8'h00; 
assign count[88]  = (CT[88]  != OCT[88])  ? count[89] + 8'h01 : count[89] + 8'h00; 
assign count[87]  = (CT[87]  != OCT[87])  ? count[88] + 8'h01 : count[88] + 8'h00; 
assign count[86]  = (CT[86]  != OCT[86])  ? count[87] + 8'h01 : count[87] + 8'h00; 
assign count[85]  = (CT[85]  != OCT[85])  ? count[86] + 8'h01 : count[86] + 8'h00; 
assign count[84]  = (CT[84]  != OCT[84])  ? count[85] + 8'h01 : count[85] + 8'h00; 
assign count[83]  = (CT[83]  != OCT[83])  ? count[84] + 8'h01 : count[84] + 8'h00; 
assign count[82]  = (CT[82]  != OCT[82])  ? count[83] + 8'h01 : count[83] + 8'h00; 
assign count[81]  = (CT[81]  != OCT[81])  ? count[82] + 8'h01 : count[82] + 8'h00; 
assign count[80]  = (CT[80]  != OCT[80])  ? count[81] + 8'h01 : count[81] + 8'h00; 
assign count[79]  = (CT[79]  != OCT[79])  ? count[80] + 8'h01 : count[80] + 8'h00; 
assign count[78]  = (CT[78]  != OCT[78])  ? count[79] + 8'h01 : count[79] + 8'h00; 
assign count[77]  = (CT[77]  != OCT[77])  ? count[78] + 8'h01 : count[78] + 8'h00;
assign count[76]  = (CT[76]  != OCT[76])  ? count[77] + 8'h01 : count[77] + 8'h00; 
assign count[75]  = (CT[75]  != OCT[75])  ? count[76] + 8'h01 : count[76] + 8'h00; 
assign count[74]  = (CT[74]  != OCT[74])  ? count[75] + 8'h01 : count[75] + 8'h00; 
assign count[73]  = (CT[73]  != OCT[73])  ? count[74] + 8'h01 : count[74] + 8'h00; 
assign count[72]  = (CT[72]  != OCT[72])  ? count[73] + 8'h01 : count[73] + 8'h00; 
assign count[71]  = (CT[71]  != OCT[71])  ? count[72] + 8'h01 : count[72] + 8'h00; 
assign count[70]  = (CT[70]  != OCT[70])  ? count[71] + 8'h01 : count[71] + 8'h00; 
assign count[69]  = (CT[69]  != OCT[69])  ? count[70] + 8'h01 : count[70] + 8'h00; 
assign count[68]  = (CT[68]  != OCT[68])  ? count[69] + 8'h01 : count[69] + 8'h00; 
assign count[67]  = (CT[67]  != OCT[67])  ? count[68] + 8'h01 : count[68] + 8'h00; 
assign count[66]  = (CT[66]  != OCT[66])  ? count[67] + 8'h01 : count[67] + 8'h00; 
assign count[65]  = (CT[65]  != OCT[65])  ? count[66] + 8'h01 : count[66] + 8'h00; 
assign count[64]  = (CT[64]  != OCT[64])  ? count[65] + 8'h01 : count[65] + 8'h00; 
assign count[63]  = (CT[63]  != OCT[63])  ? count[64] + 8'h01 : count[64] + 8'h00; 
assign count[62]  = (CT[62]  != OCT[62])  ? count[63] + 8'h01 : count[63] + 8'h00; 
assign count[61]  = (CT[61]  != OCT[61])  ? count[62] + 8'h01 : count[62] + 8'h00; 
assign count[60]  = (CT[60]  != OCT[60])  ? count[61] + 8'h01 : count[61] + 8'h00; 
assign count[59]  = (CT[59]  != OCT[59])  ? count[60] + 8'h01 : count[60] + 8'h00; 
assign count[58]  = (CT[58]  != OCT[58])  ? count[59] + 8'h01 : count[59] + 8'h00; 
assign count[57]  = (CT[57]  != OCT[57])  ? count[58] + 8'h01 : count[58] + 8'h00; 
assign count[56]  = (CT[56]  != OCT[56])  ? count[57] + 8'h01 : count[57] + 8'h00; 
assign count[55]  = (CT[55]  != OCT[55])  ? count[56] + 8'h01 : count[56] + 8'h00; 
assign count[54]  = (CT[54]  != OCT[54])  ? count[55] + 8'h01 : count[55] + 8'h00; 
assign count[53]  = (CT[53]  != OCT[53])  ? count[54] + 8'h01 : count[54] + 8'h00;  
assign count[52]  = (CT[52]  != OCT[52])  ? count[53] + 8'h01 : count[53] + 8'h00;
assign count[51]  = (CT[51]  != OCT[51])  ? count[52] + 8'h01 : count[52] + 8'h00;
assign count[50]  = (CT[50]  != OCT[50])  ? count[51] + 8'h01 : count[51] + 8'h00;
assign count[49]  = (CT[49]  != OCT[49])  ? count[50] + 8'h01 : count[50] + 8'h00;
assign count[48]  = (CT[48]  != OCT[48])  ? count[49] + 8'h01 : count[49] + 8'h00;
assign count[47]  = (CT[47]  != OCT[47])  ? count[48] + 8'h01 : count[48] + 8'h00;
assign count[46]  = (CT[46]  != OCT[46])  ? count[47] + 8'h01 : count[47] + 8'h00;
assign count[45]  = (CT[45]  != OCT[45])  ? count[46] + 8'h01 : count[46] + 8'h00;
assign count[44]  = (CT[44]  != OCT[44])  ? count[45] + 8'h01 : count[45] + 8'h00;
assign count[43]  = (CT[43]  != OCT[43])  ? count[44] + 8'h01 : count[44] + 8'h00;
assign count[42]  = (CT[42]  != OCT[42])  ? count[43] + 8'h01 : count[43] + 8'h00;
assign count[41]  = (CT[41]  != OCT[41])  ? count[42] + 8'h01 : count[42] + 8'h00;
assign count[40]  = (CT[40]  != OCT[40])  ? count[41] + 8'h01 : count[41] + 8'h00;
assign count[39]  = (CT[39]  != OCT[39])  ? count[40] + 8'h01 : count[40] + 8'h00;
assign count[38]  = (CT[38]  != OCT[38])  ? count[39] + 8'h01 : count[39] + 8'h00;
assign count[37]  = (CT[37]  != OCT[37])  ? count[38] + 8'h01 : count[38] + 8'h00;
assign count[36]  = (CT[36]  != OCT[36])  ? count[37] + 8'h01 : count[37] + 8'h00;
assign count[35]  = (CT[35]  != OCT[35])  ? count[36] + 8'h01 : count[36] + 8'h00;
assign count[34]  = (CT[34]  != OCT[34])  ? count[35] + 8'h01 : count[35] + 8'h00;
assign count[33]  = (CT[33]  != OCT[33])  ? count[34] + 8'h01 : count[34] + 8'h00;
assign count[32]  = (CT[32]  != OCT[32])  ? count[33] + 8'h01 : count[33] + 8'h00;
assign count[31]  = (CT[31]  != OCT[31])  ? count[32] + 8'h01 : count[32] + 8'h00;
assign count[30]  = (CT[30]  != OCT[30])  ? count[31] + 8'h01 : count[31] + 8'h00;
assign count[29]  = (CT[29]  != OCT[29])  ? count[30] + 8'h01 : count[30] + 8'h00;
assign count[28]  = (CT[28]  != OCT[28])  ? count[29] + 8'h01 : count[29] + 8'h00;
assign count[27]  = (CT[27]  != OCT[27])  ? count[28] + 8'h01 : count[28] + 8'h00;
assign count[26]  = (CT[26]  != OCT[26])  ? count[27] + 8'h01 : count[27] + 8'h00;
assign count[25]  = (CT[25]  != OCT[25])  ? count[26] + 8'h01 : count[26] + 8'h00;
assign count[24]  = (CT[24]  != OCT[24])  ? count[25] + 8'h01 : count[25] + 8'h00;
assign count[23]  = (CT[23]  != OCT[23])  ? count[24] + 8'h01 : count[24] + 8'h00;
assign count[22]  = (CT[22]  != OCT[22])  ? count[23] + 8'h01 : count[23] + 8'h00;
assign count[21]  = (CT[21]  != OCT[21])  ? count[22] + 8'h01 : count[22] + 8'h00;
assign count[20]  = (CT[20]  != OCT[20])  ? count[21] + 8'h01 : count[21] + 8'h00;
assign count[19]  = (CT[19]  != OCT[19])  ? count[20] + 8'h01 : count[20] + 8'h00;
assign count[18]  = (CT[18]  != OCT[18])  ? count[19] + 8'h01 : count[19] + 8'h00;
assign count[17]  = (CT[17]  != OCT[17])  ? count[18] + 8'h01 : count[18] + 8'h00;
assign count[16]  = (CT[16]  != OCT[16])  ? count[17] + 8'h01 : count[17] + 8'h00;
assign count[15]  = (CT[15]  != OCT[15])  ? count[16] + 8'h01 : count[16] + 8'h00;
assign count[14]  = (CT[14]  != OCT[14])  ? count[15] + 8'h01 : count[15] + 8'h00;
assign count[13]  = (CT[13]  != OCT[13])  ? count[14] + 8'h01 : count[14] + 8'h00;
assign count[12]  = (CT[12]  != OCT[12])  ? count[13] + 8'h01 : count[13] + 8'h00;
assign count[11]  = (CT[11]  != OCT[11])  ? count[12] + 8'h01 : count[12] + 8'h00;
assign count[10]  = (CT[10]  != OCT[10])  ? count[11] + 8'h01 : count[11] + 8'h00;
assign count[9]   = (CT[9]   != OCT[9])   ? count[10] + 8'h01 : count[10] + 8'h00;
assign count[8]   = (CT[8]   != OCT[8])   ? count[9] + 8'h01 : count[9] + 8'h00;
assign count[7]   = (CT[7]   != OCT[7])   ? count[8] + 8'h01 : count[8] + 8'h00;
assign count[6]   = (CT[6]   != OCT[6])   ? count[7] + 8'h01 : count[7] + 8'h00;
assign count[5]   = (CT[5]   != OCT[5])   ? count[6] + 8'h01 : count[6] + 8'h00;
assign count[4]   = (CT[4]   != OCT[4])   ? count[5] + 8'h01 : count[5] + 8'h00;
assign count[3]   = (CT[3]   != OCT[3])   ? count[4] + 8'h01 : count[4] + 8'h00;
assign count[2]   = (CT[2]   != OCT[2])   ? count[3] + 8'h01 : count[3] + 8'h00;
assign count[1]   = (CT[1]   != OCT[1])   ? count[2] + 8'h01 : count[2] + 8'h00;
assign count[0]   = (CT[0]   != OCT[0])   ? count[1] + 8'h01 : count[1] + 8'h00;

reg [127 : 0] fct;
reg i;
always @(count[0])
begin
if (count[0] > 8'h40)
begin
fct = ~(CT);
i = 1'b1;
end
else
begin
fct = CT;
i =1'b0;
end
end
assign FCT = fct;
assign inv = i;
endmodule



