module shiftRowInv (S1, S2);
output [127:0] S2;
input  [127:0] S1 ;
wire [7:0] x [15:0];
wire [7:0] y [15:0];
assign x[0]  = S1[7:0];
assign x[4]  = S1[15:8];
assign x[8]  = S1[23:16];
assign x[12] = S1[31:24];
assign x[1]  = S1[39:32];
assign x[5]  = S1[47:40];
assign x[9]  = S1[55:48];
assign x[13] = S1[63:56];
assign x[2]  = S1[71:64];
assign x[6]  = S1[79:72];
assign x[10] = S1[87:80];
assign x[14] = S1[95:88];
assign x[3]  = S1[103:96];
assign x[7]  = S1[111:104];
assign x[11] = S1[119:112];
assign x[15] = S1[127:120];


assign y[0]  = x[0];
assign y[1]  = x[13];
assign y[2]  = x[10];
assign y[3]  = x[7];
assign y[4]  = x[4];
assign y[5]  = x[1];
assign y[6]  = x[14];
assign y[7]  = x[11];
assign y[8]  = x[8];
assign y[9]  = x[5];
assign y[10] = x[2];
assign y[11] = x[15];
assign y[12] = x[12];
assign y[13] = x[9];
assign y[14] = x[6];
assign y[15] = x[3];

assign S2[7:0]      = y[0];
assign S2[15:8]     = y[4];
assign S2[23:16]    = y[8];
assign S2[31:24]    = y[12];
assign S2[39:32]    = y[1];
assign S2[47:40]    = y[5];
assign S2[55:48]    = y[9];
assign S2[63:56]    = y[13];
assign S2[71:64]    = y[2];
assign S2[79:72]    = y[6];
assign S2[87:80]    = y[10];
assign S2[95:88]    = y[14];
assign S2[103:96]   = y[3];
assign S2[111:104]  = y[7];
assign S2[119:112]  = y[11];
assign S2[127:120]  = y[15];

endmodule

